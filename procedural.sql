CREATE DEFINER=`rapha`@`localhost` PROCEDURE `validate_user`(
	IN idUser nvarchar(5),
    IN idProject nvarchar(5)
)
BEGIN
	DECLARE incrementValue INT DEFAULT 10;
    DECLARE increment INT DEFAULT 10;
    DECLARE rowCount INT DEFAULT 10;
    DECLARE updatedCount INT DEFAULT 10;

	CREATE TEMPORARY TABLE project_activity_id (id int); 
	INSERT INTO project_activity_id 
		SELECT arrival_activity_id
        FROM arrival_activity
		WHERE project_id = idProject;

...