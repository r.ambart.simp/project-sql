Chaque utilisateur·ice aura un compte à son nom et pourra se déclarer hôte·sse, ce qui lui demandera de renseigner les informations liées à son lieu de vie (type de lieu, d'activités, adresse, capacité d'accueil etc.)

Les hôte·sse auront également la possibilité de créer des chantiers à des dates spécifiques, pour un nombre de personne donné et d'y inscrire les types d'activités qui seront pratiquées lors de ce chantier.

Les volontaires pourront consulter la liste des hôtes·ses ainsi que celle des chantiers à venir et soit faire une demande de "WOOFING" à un·e hôte·sse, soit directement s'inscire sur un chantier existant.

Un·e hôte·sse pourra valider la présences des différent·e·s volontaires le jour du chantier.

Le fait de participer à des chantiers fait monter le niveau de compétence du/de la volontaire liée aux activités pratiquées.

Un·e utilisateur·ice peut consulter dans son profil la liste des chantiers et des WOOFing effectués (ou non, si absent·e). Ielle peut également voir son niveau de compétence dans les différentes activités selon ses participation passées.


![alt text](diagram.png "Logo Title Text 1")
