CREATE DATABASE project_sql;

USE project_sql;

-- Creation table user

CREATE TABLE `user`
(
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `username` varchar(50) NOT NULL,
    `arrival_id` INT
);

-- Creation table arrival

CREATE TABLE `arrival` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `address` VARCHAR(200) NOT NULL,
  `capacity` INT NOT NULL,
  `type` TEXT NULL);


-- FK Arrival-User

ALTER TABLE `user`
    ADD CONSTRAINT FK_user_arrival FOREIGN KEY(`arrival_id`) 
    REFERENCES `arrival`(`id`);


-- Creation table skill

CREATE TABLE `skill`
(
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `level` INT,
    `user_id` INT,
    `activities_id` INT
);

-- FK Skill-User

ALTER TABLE `skill`
    ADD CONSTRAINT `FK_user_skill` FOREIGN KEY(`user_id`) 
    REFERENCES `user`(`id`);


-- Creation table activities

CREATE TABLE `activities` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `label` VARCHAR(100) NOT NULL
);


-- FK Skill - activities

ALTER TABLE `skill`
    ADD CONSTRAINT `FK_skill_activities` FOREIGN KEY(`activities_id`) 
    REFERENCES `activities`(`id`);


-- Creation table Arrival_activities

CREATE TABLE `arrival_activities` (
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `arrival_id` INT,
    `activities_id` INT 
);

-- FK Project - Arrival_activities

    ALTER TABLE `arrival_activities`
    ADD CONSTRAINT `FK_arrival_activities` FOREIGN KEY(`arrival_id`) 
    REFERENCES `arrival`(`id`);


-- FK arrival Activities - Project

    ALTER TABLE `arrival_activities`
    ADD CONSTRAINT `FK_project_activities_arrival` FOREIGN KEY(`activities_id`) 
    REFERENCES `activities`(`id`);

-- Creattion table Project

CREATE TABLE `project` (
  `id` INT PRIMARY KEY AUTO_INCREMENT,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  `places` INT NOT NULL,
  `arrival_id` INT 
);

-- FK Project - Arrival

    ALTER TABLE `project`
    ADD CONSTRAINT `FK_project_arrival` FOREIGN KEY(`arrival_id`) 
    REFERENCES `arrival`(`id`);


-- Creation table de jonction Project_activities

CREATE TABLE `project_activities` (
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `project_id` INT, 
    `activities_id` INT 
);

-- FK Project - Arrival_activities

ALTER TABLE `project_activities`
    ADD CONSTRAINT `FK_project_activities_project` FOREIGN KEY(`project_id`) 
    REFERENCES `project`(`id`);

-- FK arrival Activities - Project

ALTER TABLE `project_activities`
    ADD CONSTRAINT `FK_project_activities_activities` FOREIGN KEY(`activities_id`) 
    REFERENCES `activities`(`id`);

-- Creation table Submit

CREATE TABLE `submit` (
    `id` INT PRIMARY KEY AUTO_INCREMENT,
    `start_date` DATE,
    `end_date` DATE,
    `is_accepted` BIT,
    `project_id` INT,
    `user_id` INT, 
    `arrival_id` INT
);

-- FK Submit-Project

ALTER TABLE `submit`
    ADD CONSTRAINT `FK_project_submit` FOREIGN KEY(`project_id`) 
    REFERENCES `project`(`id`);

-- FK Submit-User

ALTER TABLE `submit`
    ADD CONSTRAINT `FK_user_submit` FOREIGN KEY(`user_id`) 
    REFERENCES `user`(`id`);

-- FK Arrival-User

ALTER TABLE `submit`
    ADD CONSTRAINT `FK_submit_arrival` FOREIGN KEY(`arrival_id`) 
    REFERENCES `user`(`id`);
