USE project_sql

INSERT INTO 'arrival' (`address`, `capacity`, `type`) VALUES
    ('1, avenue de teste', '10', 'maison'),
    ('2, boulevard de teste', '11', null);


INSERT INTO 'user' (`username`, `password`, `arrival_id`) VALUES 
    ('teste test', '1234', 1),
    ('test test', '1234', 2);


INSERT INTO `activities` (`label`) VALUES 
    ('teste'),
    ('labeltest');


INSERT INTO `project_sql`.`arrival_activities` (`arrival_id`, `activities_id`) VALUES 
    ('1','2'), ('1','2');



INSERT INTO `skill` (`level`, `user_id`, `activities_id`) VALUES 
    ('100','1','1'),
    ('200','2','2');

INSERT INTO `project` (`start_date`, `end_date`, `capacity`, `arrival_id`) VALUES 
    ('2008-12-12','2012-12-12','10','1'),
    ('2009-12-12','2012-12-12','12','2');



INSERT INTO `submit` (`start_date`, `end_date`, `isAccepted`, `project_id`, `user_id`, `arrival_id`) VALUES 
    ('2010-12-12','2011-12-12',1,'1','1','1'),
    ('2010-12-12','2011-12-12',0,'2','2','2');



